/*
FILE:		CSVconversion.js
AUTHOR:		Brody Harris

SYNOPSIS:
----------
This script is designed to run in the Alfresco Javascript console.This script will search each sites dataList folder for existing datalists, extract their data, and output it into a new folder named createdDataLists as a .csv file for external use.

The script is comprised of 4 main fucntions:
---------------------------------------------
findSites() 	- Returns an array of sites on this instance of Fresh Project
findDataLists()	- Returns an array of pmodl: type Datalists found on the specified site
getDLContent()	- Returns a String (formatted for csv output) that contains all of the data for the specified dataList (Checks for type of list passed, calls one of three sub functions)
-getTypeContent()	- Returns array of list data specific to type
exportCSV()		- Exports any string given to csv on the given site with the given list name
*/

if (space.name != "Sites"){
	throw new Error("Not running in sites folder!");	
}

//Get sites
var sites = findSites();

for (var s in sites){
	
	//Print empty line here to seaprate the search of each site in output
	print(" ");
	
	//Run through the main logic
	var site = sites[s]
	
	var lists = findDataLists(site);
	
	for (var l in lists ){
		
		var list = lists[l];
		
		//print(getDLContent(list));
		
		exportCSV(getDLContent(list), list, site);
	}
}

//	FIND THE SITES IN THE FP INSTANCE
//		Returns an array of sites
function findSites(){
	
	print("Finding sites...");
	//sites from space
	var sites = space.getChildren();
	//Check to see if there are sites
	if(sites.length === 0){
		//throw critical error if no sites found
		throw new Error("No sites found!");
	}
	//Check for non sites found in sites folder
	print("Checking for non-sites in sites folder...");
	sites = sites.filter(function filterSites(site){
		if (!site.type.match("site")){
			print(site.properties.title + " is not a site, removed");
			return false;
		}
		return true;
	});
	
	//return sites
	print(sites.length + " sites found");
	return sites;	
	
}

//	FIND THE DATALISTS IN A SITE
//		Requires a Site as parameter
//		Returns an array of dataLists
function findDataLists(site){
	
	print(" Finding DataLists in site \"" + site.properties.title + "\"...");
	//get site folders
	var siteContent = site.getChildren();
	//look for a dataLists folder in each site
	for (var f in siteContent) {
		folder = siteContent[f];
		if (folder.properties.name.match("dataLists")){
			var dlFolder = folder;
			break;
		}
	}
	//make sure datalist folder was found
	if (dlFolder === undefined){
		print(" No dataLists folder found");
		return [];
	} 
	//check if datalist folder has content
	var lists = dlFolder.getChildren();
	if (lists.length === 0){
		print(" No dataLists found");
		return [];
	}
	//Check for non sites found in sites folder
	print("Checking for non-lists in lists folder...");
	lists = lists.filter(function filterSites(list){
		
		var type = list.getProperties()["{http://www.alfresco.org/model/datalist/1.0}dataListItemType"];
		var children = list.getChildren();
		
		if (!list.type.match("list")){
			print(list.properties.title + " is not a list, removed");
			return false;
		} else if ( type.toString().indexOf("pmodl") < 0 ) {
			print(list.properties.title + " is not a Fresh-Project type DataList, removed");
			return false;
		} else if (children.length === 0){
			print (list.properties.title + " does not have any content, removed");
			return false;
		}
		return true;
	});
	
	//Return filtered results
	print(" Found " + lists.length + " valid list(s)");
	return lists;
}

//	GET THE DATA FROM A DATALIST
//		Requires a dataList as parameter
//		Returns a string for csv output
function getDLContent(list){
	//Check Type of list, call the respective functions
	switch(list.getProperties()["{http://www.alfresco.org/model/datalist/1.0}dataListItemType"]){
	
		case "pmodl:issuesList":
			var fields = ["issueID","issueTitle","issueOrigDate","issueDescription","issuePriority","issueImpact","issueMititationDesc","issueDueDate","issueCompletionDate","issueStatus"];
			var columns = "Issue ID,Title,Origination Date,Description,Priority,Impact,Mitigation Description,Due Date,Completion Date,Status,Assigned,Attachments\n";
			return getTypeContent(list, columns, fields, "pmodl:issuesList");
			break;
			
		case "pmodl:risksList":
			var fields = ["riskNumber","riskTitle","riskOrigDate","riskDescription","riskPriority","riskImpact","riskMititationDesc","riskDueDate","riskCompletionDate","riskStatus"];
			var columns = "Item Number,Title,Origination Date,Description,Priority,Impact,Mitigation Description,Due Date,Completion Date,Status,Assigned,Attachments\n";
			return getTypeContent(list, columns, fields, "pmodl:risksList");
			break;
			
		case "pmodl:projectStatus":
			var fields = ["varianceStoplight","projectName","projectStatus","budget","actuals","etc"];
			var columns = "StopLight,Project Name,Status,Budget,Actuals,Estimated to Completion,Estimated at Completion,Variance,Percent Complete,Percent Expended,Comments\n";
			return getTypeContent(list, columns, fields, "pmodl:projectStatus");
			break;
	}
}



function getTypeContent(list, columns, fields, type){
	
	print("  Getting list Data from \"" + list.properties.title + "\"...");
	
	//Get rows from array
	var rows = list.getChildren();
	print("  Found " + rows.length + " rows");
	
	//Create rows array for storage
	var rowArray = [];
	
	//cycle through rows
	for (var o in rows){

		//get row properties
		var rowData = rows[o].getProperties();

		//Make new array for this row's data
		var properties = [];

		//Add properties to row array, replacing all double quotes with 2 double quotes
		for (var f in fields){
			
			properties.push("\"" + String(rowData["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}" + fields[f]]).replace(/"/g, "\"\"") + "\"");
		}
		
		//is project status type, calculate pm values
		if ( type.indexOf("pmodl:projectStatus") == 0.0 ){
			
			//Calculations
			var actuals = rowData["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}actuals"];
			var budget =  rowData["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}budget"];
			var etc =  rowData["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}etc"];
			
			var eac = actuals + etc;
			var variance = budget - eac;
			var perComplete = actuals / eac * 100;
			var perExpended = actuals / budget * 100;
			
			properties.push(eac.toFixed(2));
			properties.push(variance.toFixed(2));
			properties.push(perComplete.toFixed(2));
			properties.push(perExpended.toFixed(2));	
			
			//Comments property (External for CSV order)
			properties.push("\"" + String(rowData["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}projectStatusComment"]).replace(/"/g, "\"\"") + "\"");
			
		//needs associations	
		} else {
			
			//Get associations
			var rowAssoc = rows[o].getAssociations();
			
			if ( type.indexOf("pmodl:risksList") == 0.0 ){
				var assignee = rowAssoc["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}assignee"];
				var attachment = rowAssoc["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}attachments"];	
			} else {
				var assignee = rowAssoc["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}issueAssignee"];	
				var attachment = rowAssoc["{http://www.ziaconsulting.com/model/pmoDataLists/1.0}issueAttachment"];	
			}
			
			
			//List Assignee names / groups
			if ( assignee !== null){
				var assignees = "\"";				
				for (p in assignee){
					
					//group or assignee
					if( assignee[p].properties.authorityDisplayName == null){
						assignees = assignees + assignee[p].properties.firstName + " " + assignee[p].properties.lastName;
					} else {
						assignees = assignees + assignee[p].properties.authorityDisplayName;
					}
					
					if ( (assignee.length - 1) > p ){
						assignees = assignees + ", ";
					}
				}
				assignees = assignees + "\"";
			} else {
				assignees = "\"\"";
			}
			properties.push(assignees);
			
			//List attachment names
			if ( attachment !== null){
				var attachments = "\"";
				for (f in attachment){
					attachments = attachments + attachment[f].properties.name + ": https://cm.ziaconsulting.com/share/page/context/mine/document-details?nodeRef=" + attachment[f].getNodeRef();
					if ( (attachment.length - 1) > f ){
						attachments = attachments + ", ";
					}
				}
				attachments = attachments + "\"";
			} else {
				attachments = "\"\"";
			}
			properties.push(attachments);
			
			
		}
	
		//Filter nulls
		for ( p in properties){

			if( properties[p].indexOf("null") > -1){
				properties[p] = "";
			}
		}
		
		//Add row array to list array
		rowArray.push(properties);
	}
	
	
	//Separate Rows(Arrays) and add them to the string
	rowArray.forEach(function(row){
		columns += row.join(",") + "\n";
	});
	return columns;
	
}

//	EXPORT CONTNENT TO CSV
//	    Requires	1. A string containing CSV formatted data from a datalist
//			2. The datalist the data originated from
//			3. The site the datalist originated from
function exportCSV(data, list, site){
	print("   Exporting data from \"" + list.properties.title + "\" to .csv...");
	//Get existing folder
	var folderOut = site.childByNamePath("documentLibrary").childByNamePath("convertedFreshProjectDataLists");
	//check if folder exists
	if (folderOut == null){
		//create new file and folder
		folderOut  = site.childByNamePath("documentLibrary").createFolder("convertedFreshProjectDataLists");
		print("   Created new \"convertedFreshProjectDataLists\" folder in documentLibrary index for csv");
	} else {
		print("   Output folder exists");	
	}
	//Check if file exists
	var fileName = list.properties.title.replace(/:/g, ";").replace(/\//g, "-");
	var csvOut = folderOut.childByNamePath(fileName + ".csv");
	if (csvOut == null){
		//Create new file
		var csvOut = folderOut.createFile(fileName + ".csv");
		csvOut.content = data;
		print("   Output to \"" + fileName + ".csv\" in \"convertedFreshProjectDataLists\" folder in documentLibrary index on site \"" + site.properties.title + "\"");
	} else {
		print("   Output file exists");
		//Change content
		csvOut.content = data;
		print("   Content updated");
	}
	csvOut.save();
}
